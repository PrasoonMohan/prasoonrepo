package cognimaven;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentAventReporter;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.ExtentKlovReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class MainClassTest {
	
	 static Logger logger = Logger.getLogger(MainClassTest.class);
	 ExtentReports extent ;
	 public ExtentHtmlReporter htmlReporter;
	 public ExtentTest Extlogger;

	
	 
	
	
	  @BeforeTest public void setup() { String log4jConfigFile =
	  System.getProperty("user.dir") + File.separator +
	  "//src//main//resources//log4j.properties";
	  PropertyConfigurator.configure(log4jConfigFile);
	  logger.debug("this is a debug log message");
	  logger.info("this is a information log message");
	  logger.warn("this is a warning log message");
	  
	  htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/STMExtentReport.html");
  	// Create an object of Extent Reports
	extent = new ExtentReports();  
	extent.attachReporter(htmlReporter);
	extent.setSystemInfo("Host Name", "SoftwareTestingMaterial");
	extent.setSystemInfo("Environment", "Production");
    extent.setSystemInfo("User Name", "Rajkumar SM");
    htmlReporter.config().setDocumentTitle("Title of the Report Comes here "); 
        // Name of the report
    htmlReporter.config().setReportName("Name of the Report Comes here "); 
        // Dark Theme
    htmlReporter.config().setTheme(Theme.STANDARD);		
    
	  }
	  
	  @Test public void testMethod1() {
      //ExtentTest test = extent.createTest("MyFirstTest   : TEstmethod 1");
		  
	  System.out.println("First testNG test Success : TestMethod 1 passed");
	  logger.info("this is a information log message : TestMethod 1 passed"); 
	  
	  
	  Extlogger = extent.createTest("MyFirstTest   :Extent test: TEstmethod 1");
	  Extlogger.log(Status.PASS, "Step 1 ExtTEst 1 passed");
	  Extlogger.info("Test step 2 passed");
	  Extlogger.log(Status.FAIL, "Step 3 ExtTEst 1 passed");
		//Assert.assertEquals(driver.getTitle(),"Google");
	  

	  
	  }
	  
	  @Test public void testMethod2() {
		  
		 // ExtentTest test = extent.createTest("MyFirstTest   : TEstmethod 2");
	  System.out.println("Second testNG test Success : TestMethod 2 passed");
	  logger.info("this is a information log message : TestMethod 2 passed 2");
	 // test.log(Status.PASS, "TEst 2 passed");
	  
	  Extlogger = extent.createTest("MySecondTest   :Extent test: TEstmethod 2");
	  Extlogger.log(Status.PASS, " ExtTEst 2 passed");
	  }
	
	  @Test public void testMethod3() {
		  System.out.println("Second test NG test Success : TestMethod 3 passed tes1 example");
		  logger.info("this is a information log message : TestMethod 3 passed"); 
		 // ExtentTest test = extent.createTest("MyFirstTest   : TEstmethod 2");
		 // test.log(Status.PASS, "TEst 2 passed");
		  
		  Extlogger = extent.createTest(" ExtThird   :Extent test: TEstmethod 2");
		  Extlogger.log(Status.PASS, " ExtTEst 3 passed");
		  
	  }
	  
	  @AfterMethod
	  public void getResult(ITestResult result) throws Exception{
	  if(result.getStatus() == ITestResult.FAILURE){
	  //MarkupHelper is used to display the output in different colors
	  Extlogger.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
	  Extlogger.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
	  //To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
	  //We do pass the path captured by this method in to the extent reports using "logger.addScreenCapture" method. 
	  //String Scrnshot=TakeScreenshot.captuerScreenshot(driver,"TestCaseFailed");
	//  String screenshotPath = getScreenShot(driver, result.getName());
	  //To add it in the extent report 
	 // Extlogger.fail("Test Case Failed Snapshot is below " + logger.addScreenCaptureFromPath(screenshotPath));
	  }
	  else if(result.getStatus() == ITestResult.SKIP){
		  Extlogger.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE)); 
	  } 
	  else if(result.getStatus() == ITestResult.SUCCESS)
	  {
		  Extlogger.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
	  }
	  //driver.quit();
	  }
	  
		@AfterTest
		public void endReport() {
			 extent.flush();
			 }
}


/*
 * public static WebDriver driver= null;
 * 
 * public static void main(String[] args) { // TODO Auto-generated method stub
 * System.out.println("starting test");
 * 
 * 
 * System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); String
 * path = System.getProperty("user.dir"); //String otherFolder = path
 * +"\\other"; System.setProperty("webdriver.chrome.driver",
 * path+"\\src\\main\\resources\\chromedriver.exe"); driver = new
 * ChromeDriver(); // final WebDriver driver= new ChromeDriver();
 * driver.get("http://www.google.com");
 * 
 * WebElement searchTextBox =
 * driver.findElement(By.xpath("//input[@class='gLFyf gsfi' and @type = 'text']"
 * ));
 * 
 * searchTextBox.sendKeys("Gmail");
 * 
 * searchTextBox.sendKeys(Keys.RETURN); }
 */

